#help:	@ List available tasks on this project
help:
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(MAKEFILE_LIST) | tr -d '#' | awk 'BEGIN {FS = ":.*?@ "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

#start:	@ Execute the main program
start:
	python3 main.py

#listen: @ Execute the UDP listener
listen:
	python3 listener.py

#test: @ Execute unit tests
test:
	pytest
