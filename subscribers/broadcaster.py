import logging
import json
import socket


class Broadcaster(object):
    def __init__(self, pub, topic):
        self._pub = pub
        self._topic = topic
        self._logger = logging.getLogger(__name__)

    def start(self):
        self._init_socket()

        self._pub.subscribe(self._publish, self._topic)

    def _init_socket(self):
        self._hostname = socket.gethostname()

        self._server = socket.socket(
            socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        # enable port reuse so we can run multiple clients and servers on a single (host, port)
        self._server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        # enable udp broadcast on the socket
        self._server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    def _publish(self, event):
        event.source = self._hostname

        json_message = json.dumps(event.__dict__)
        message = bytes(json_message, 'utf-8')

        try:
            self._server.sendto(message, ("<broadcast>", 37020))
        except:
            self._logger.error('Unable to broadcast on the network.')
