class HatUpdater(object):
    def __init__(self, hat_controller, pub, topic):
        self._controller = hat_controller
        self._pub = pub
        self._topic = topic

    def start(self):
        self._pub.subscribe(self._handle_sample, self._topic)

    def _handle_sample(self, event):
        self._controller.handle_sample(event)
