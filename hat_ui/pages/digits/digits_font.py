digits = [
    [
        # 0
        False, True, True, True,
        False, True, False, True,
        False, True, False, True,
        False, True, False, True,
        False, True, True, True
    ], [
        # 1
        False, False, True, False,
        False, True, True, False,
        False, False, True, False,
        False, False, True, False,
        False, True, True, True
    ], [
        # 2
        False, True, True, True,
        False, False, False, True,
        False, False, True, False,
        False, True, False, False,
        False, True, True, True
    ], [
        # 3
        False, True, True, True,
        False, False, False, True,
        False, False, True, True,
        False, False, False, True,
        False, True, True, True
    ], [
        # 4
        False, True, False, True,
        False, True, False, True,
        False, True, True, True,
        False, False, False, True,
        False, False, False, True
    ], [
        # 5
        False, True, True, True,
        False, True, False, False,
        False, True, True, True,
        False, False, False, True,
        False, True, True, True,
    ], [
        # 6
        False, True, True, True,
        False, True, False, False,
        False, True, True, True,
        False, True, False, True,
        False, True, True, True
    ], [
        # 7
        False, True, True, True,
        False, False, False, True,
        False, False, True, False,
        False, True, False, False,
        False, True, False, False
    ], [
        # 8
        False, True, True, True,
        False, True, False, True,
        False, True, True, True,
        False, True, False, True,
        False, True, True, True
    ], [
        # 9
        False, True, True, True,
        False, True, False, True,
        False, True, True, True,
        False, False, False, True,
        False, True, True, True
    ]
]
