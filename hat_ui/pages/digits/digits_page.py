from time import sleep
from hat_ui.pages.digits.digits_font import digits

class DigitsPage(object):
    def __init__(self, sense, themes):
        self._sense = sense
        self._themes = themes
        self._prev_temp = 0
        self._theme_index = 0
        self._theme = self._themes[self._theme_index]

    def set_next_theme(self):
        if self._theme_index >= len(self._themes) - 1:
            self._theme_index = 0
        else:
            self._theme_index += 1

        self._theme = self._themes[self._theme_index]

    def paint(self, temperature):
        '''Update the led screen with the given temperature'''
        grid = self.build_grid(temperature)

        ping_grid = self.set_ping(temperature, grid)
        self._sense.set_pixels(ping_grid)
        sleep(.2)

        self._sense.set_pixels(grid)

    def set_ping(self, temperature, grid):
        # take the final grid, make a copy and display a dot in the corner briefly to show we got an update
        ping = grid.copy()

        if temperature > self._prev_temp:
            color = self._theme.ping_hotter_color
        elif temperature < self._prev_temp:
            color = self._theme.ping_colder_color
        else:
            color = self._theme.ping_static_color

        ping[0] = color

        self._prev_temp = temperature

        return ping

    def build_grid(self, temperature):
        '''Build the 64-element array to feed to the pi-hat's led array. It will display the temperature as digits on a single "screen".'''
        # initialize grid to all background color
        led_grid = [self._theme.background_color] * 64

        self.populate_whole_number(led_grid, temperature)
        self.populate_tenths_indicator(led_grid, temperature)
        self.populate_hundred_indicator(led_grid, temperature)
        self.populate_negative_indicator(led_grid, temperature)

        return led_grid

    @staticmethod
    def get_digit(number, n):
        '''Return the nth position of number as an integer, where 10 to the n will give you the place of the digit you want (e.g 10**0 is the 1's place)'''
        num = abs(number)

        if n < 0:
            # if you want the fractional part, first move the decimal just after the digit you want
            # then execute the function for the ones place
            return DigitsPage.get_digit(num * 10**abs(n), 0)
        else:
            return int(num // 10**n % 10)

    def populate_whole_number(self, led_grid, temperature):
        '''Fills the top 5 lines of the grid with the tens and ones place digits of the temperature'''
        tens_grid = digits[DigitsPage.get_digit(temperature, 1)]
        ones_grid = digits[DigitsPage.get_digit(temperature, 0)]

        # first five rows will be the digits
        for y in range(5):
            # each digit is 4 pixels wide, but two side by side fill the width of the grid
            for x in range(4):
                # tens digit
                digit_index = y*4 + x
                grid_index = (y + 1)*8 + x

                if (tens_grid[digit_index]):
                    led_grid[grid_index] = self._theme.tens_color

                # ones digit (covers the right half of the grid)
                if (ones_grid[digit_index]):
                    led_grid[grid_index + 4] = self._theme.ones_color

    def populate_tenths_indicator(self, led_grid, temperature):
        '''Show the tenths digit of the temperatur as a graph along the bottom of the led array'''
        tenths = DigitsPage.get_digit(temperature, -1)

        color = self._theme.tenths_color
        accent_color = self._theme.tenths_highlight_color

        # when we get to 9, we hightlight the 8 position since we only have 8
        # and wrapping looks terrible
        if tenths > 0:
            led_grid[56] = color
        if tenths > 1:
            led_grid[57] = color
        if tenths > 2:
            led_grid[58] = color
        if tenths > 3:
            led_grid[59] = color
        if tenths > 4:
            led_grid[60] = color
        if tenths > 5:
            led_grid[61] = color
        if tenths > 6:
            led_grid[62] = color
        if tenths > 7:
            led_grid[63] = color
        if tenths > 8:
            led_grid[63] = accent_color

    def populate_negative_indicator(self, led_grid, temperature):
        '''Show bar on bottom for negative temperature'''
        color = self._theme.neg_sign_color

        if temperature < 0:
            led_grid[0] = color
            led_grid[1] = color

    def populate_hundred_indicator(self, led_grid, temperature):
        '''squeeze in 1 for hundreds digit if exists'''
        hundreds = DigitsPage.get_digit(temperature, 2)

        color = self._theme.hundreds_color

        if hundreds == 1:
            led_grid[0] = color
            led_grid[8] = color
            led_grid[16] = color
