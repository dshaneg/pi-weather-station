from hat_ui.pages.digits import colors
from hat_ui.pages.digits.digits_font import digits


class DigitsTheme(object):
    def __init__(self, name,
                 background=colors.BLACK,
                 hundreds=colors.RED, tens=colors.BLUE, ones=colors.BLUE,
                 tenths=colors.BLUE, tenths_highlight=colors.LT_BLUE,
                 neg_sign=colors.RED,
                 ping_colder=colors.DK_BLUE, ping_hotter=colors.DK_RED, ping_static=colors.GRAY_50
                 ):
        self.name = name
        self.background_color = background
        self.hundreds_color = hundreds
        self.tens_color = tens
        self.ones_color = ones
        self.tenths_color = tenths
        self.tenths_highlight_color = tenths_highlight
        self.neg_sign_color = neg_sign
        self.ping_colder_color = ping_colder
        self.ping_hotter_color = ping_hotter
        self.ping_static_color = ping_static


themes = [
    DigitsTheme('blue'),
    DigitsTheme('candy',
                background=colors.BLACK,
                hundreds=colors.YELLOW,
                tens=colors.CYAN,
                ones=colors.MAGENTA,
                tenths=colors.YELLOW,
                tenths_highlight=colors.LT_GREEN,
                neg_sign=colors.RED,
                ping_colder=colors.DK_BLUE,
                ping_hotter=colors.DK_RED,
                ping_static=colors.DK_GREEN),
    DigitsTheme('red',
                background=colors.BLACK,
                hundreds=colors.YELLOW,
                tens=colors.RED,
                ones=colors.RED,
                tenths=colors.LT_RED,
                tenths_highlight=colors.DK_RED,
                neg_sign=colors.YELLOW,
                ping_colder=colors.GRAY_25,
                ping_hotter=colors.GRAY_25,
                ping_static=colors.GRAY_25),
    DigitsTheme('banana',
                background=colors.YELLOW,
                tens=colors.GRAY_25,
                ones=colors.GRAY_25,
                tenths=colors.GRAY_25,
                tenths_highlight=colors.BLACK),
]
