from time import sleep

black = (0, 0, 0)
white = (255, 255, 255)
ltgray = (125, 125, 125)
dkgray = (75, 75, 75)

red = (255, 0, 0)
blue = (0, 0, 255)
yellow = (255, 255, 0)
cyan = (0, 255, 255)
brown = (102, 51, 0)
tan = (205, 133, 63)

green = (0, 255, 0)
ltgreen = (175, 255, 175)
dkgreen = (0, 100, 0)
medgreen = (0, 200, 0)

MAX_COMFORTABLE_TEMP = 74.0
MIN_COMFORTABLE_TEMP = 69.5
VERY_COLD_TEMP = 68
VERY_HOT_TEMP = 76


class FacesPage(object):
    def __init__(self, sense):
        self._sense = sense

    def set_next_theme(self):
        pass

    def paint(self, temperature):
        if temperature >= VERY_HOT_TEMP:
            self.show_very_hot()
        elif temperature >= MAX_COMFORTABLE_TEMP:
            self.show_hot()
        elif temperature >= MIN_COMFORTABLE_TEMP:
            self.show_normal()
        elif temperature >= VERY_COLD_TEMP:
            self.show_cold()
        else:
            self.show_very_cold()

    def show_very_hot(self):
        self._show_creeper_face(color=red, eye_color=yellow)
        sleep(.2)
        self._show_creeper_face(color=red, eye_color=black)

    def show_hot(self):
        self._show_zombie_face(eye_color=red)
        sleep(.2)
        self._show_zombie_face(eye_color=black)

    def show_normal(self):
        self._show_steve_face(eye_color=white, eye_background=blue)
        sleep(.4)
        self._show_steve_face(eye_color=blue, eye_background=white)

    def show_cold(self):
        self._show_skeleton_face(eye_color=red)
        sleep(.2)
        self._show_skeleton_face(eye_color=black)

    def show_very_cold(self):
        self._show_creeper_face(color=cyan, eye_color=blue)
        sleep(.2)
        self._show_creeper_face(color=cyan, eye_color=black)

    def _show_steve_face(self, eye_color, eye_background):
        H = brown  # hair
        E = eye_color
        S = tan  # skin
        e = eye_background

        # Set up where each colour will display
        pixels = [
            H, H, H, H, H, H, H, H,
            H, H, H, H, H, H, H, H,
            H, S, S, S, S, S, S, H,
            S, S, S, S, S, S, S, S,
            S, e, E, S, S, e, E, S,
            S, S, S, H, H, S, S, S,
            S, S, H, S, S, H, S, S,
            S, S, H, H, H, H, S, S
        ]

        # Display these colours on the LED matrix
        self._sense.set_pixels(pixels)

    def _show_skeleton_face(self, eye_color):
        g = ltgray
        G = dkgray
        w = white
        e = eye_color
        b = black  # mouth color

        # Set up where each colour will display
        pixels = [
            g, g, G, g, g, g, g, g,
            g, w, w, G, w, w, g, g,
            w, w, w, G, w, w, w, w,
            w, w, w, w, w, w, w, w,
            w, e, e, w, w, e, e, w,
            w, w, w, G, G, w, w, w,
            g, b, b, b, b, b, b, g,
            w, w, w, w, w, w, w, w,
        ]

        # Display these colours on the LED matrix
        self._sense.set_pixels(pixels)

    def _show_zombie_face(self, eye_color):
        g = ltgreen
        G = dkgreen
        m = medgreen
        e = eye_color

        # Set up where each colour will display
        pixels = [
            G, G, G, G, G, G, G, m,
            G, G, m, g, g, m, G, G,
            m, g, g, g, g, g, g, m,
            g, m, g, g, g, g, m, g,
            m, e, e, g, m, e, e, m,
            m, g, g, G, G, g, g, m,
            m, g, G, m, m, G, m, g,
            m, m, G, G, m, G, m, m,
        ]

        # Display these colours on the LED matrix
        self._sense.set_pixels(pixels)

    def _show_creeper_face(self, color, eye_color):
        S = color  # skin color
        e = eye_color
        m = black  # mouth color

        # Set up where each colour will display
        pixels = [
            S, S, S, S, S, S, S, S,
            S, S, S, S, S, S, S, S,
            S, e, e, S, S, e, e, S,
            S, e, e, S, S, e, e, S,
            S, S, S, m, m, S, S, S,
            S, S, m, m, m, m, S, S,
            S, S, m, m, m, m, S, S,
            S, S, m, S, S, m, S, S
        ]

        # Display these colours on the LED matrix
        self._sense.set_pixels(pixels)
