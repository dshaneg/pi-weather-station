from sense_hat.stick import ACTION_RELEASED

class HatController(object):
    def __init__(self, sense, page_service):
        self._sense = sense
        self._page_service = page_service

        self._latest_temp = 0
        self._is_display_on = True

        self._hook_stick_events()

    def _hook_stick_events(self):
        self._sense.stick.direction_middle = self._handle_stick_middle
        self._sense.stick.direction_down = self._handle_stick_down
        self._sense.stick.direction_up = self._handle_stick_up
        self._sense.stick.direction_left = self._handle_stick_left
        self._sense.stick.direction_right = self._handle_stick_right

    def handle_sample(self, event):
        self._latest_temp = event.temperature

        self._paint_current()

    def _paint_current(self):
        # in most cases, if display is on, the correct face will be showing
        # however, this handles the case where we cross a threshold
        if self._is_display_on:
            self._page_service.paint(self._latest_temp)

    def _handle_stick_middle(self, stick_event):
        if stick_event.action != ACTION_RELEASED:
            return

        self._toggle_display()

    def _toggle_display(self):
        if self._is_display_on:
            self._turn_display_off()
        else:
            self._turn_display_on()

    def _turn_display_off(self):
        self._sense.clear()
        self._is_display_on = False

    def _turn_display_on(self):
        self._is_display_on = True

        self._paint_current()

    def _handle_stick_down(self, stick_event):
        if stick_event.action != ACTION_RELEASED:
            return

        self._sense.low_light = not self._sense.low_light

    def _handle_stick_up(self, stick_event):
        if stick_event.action != ACTION_RELEASED:
            return

        self._page_service.set_next_theme()
        self._turn_display_on()

    def _handle_stick_left(self, stick_event):
        if stick_event.action != ACTION_RELEASED:
            return

        self._page_service.set_previous()
        self._turn_display_on()

    def _handle_stick_right(self, stick_event):
        if stick_event.action != ACTION_RELEASED:
            return

        self._page_service.set_next()
        self._turn_display_on()
