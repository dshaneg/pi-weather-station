class PageService(object):
    def __init__(self, pages):
        self._pages = pages

        self._current_page_index = 0

    def get_current(self):
        return self._pages[self._current_page_index]

    def paint(self, temp):
        self.get_current().paint(temp)

    def set_next_theme(self):
        self.get_current().set_next_theme()

    def set_previous(self):
        if self._current_page_index == 0:
            self._current_page_index = len(self._pages) - 1
        else:
            self._current_page_index -= 1

    def set_next(self):
        if self._current_page_index == len(self._pages) - 1:
            self._current_page_index = 0
        else:
            self._current_page_index += 1
