import logging
from datetime import datetime, timezone
import sched
import time

from measurements.sampler_event import SamplerEvent
from measurements.converter import to_fahrenheit, to_inches


class Sampler(object):
    def __init__(self, sense, pub, topic):
        self._sense = sense
        self._pub = pub
        self._topic = topic
        self._scheduler = sched.scheduler(time.time, time.sleep)
        self._priority = 2
        self._logger = logging.getLogger(__name__)

    def start(self, interval_secs):
        self._delay = interval_secs
        self._schedule_initial_sample()
        self._scheduler.run()

    def _log_event(self, event):
        message = f'{event.timestamp}: Temperature: {event.temperature}°F Humidity: {event.humidity}% Pressure: {event.pressure} in.'
        self._logger.info(message)

    def _sample(self):
        self._schedule_next_sample()

        event = self._read_sensors()

        self._log_event(event)
        self._pub.sendMessage(self._topic, event=event)

    def _schedule_initial_sample(self):
        self._scheduler.enter(0, self._priority, self._sample)

    def _schedule_next_sample(self):
        self._scheduler.enter(self._delay, self._priority, self._sample)

    def _read_sensors(self):
        temp = round(to_fahrenheit(self._sense.get_temperature_from_humidity()), 1)
        humidity = round(self._sense.get_humidity(), 1)
        pressure = round(to_inches(self._sense.get_pressure()), 1)

        raw_stamp = datetime.now(timezone.utc)
        stamp = raw_stamp.isoformat()

        return SamplerEvent(stamp, temp, humidity, pressure)
