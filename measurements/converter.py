def to_fahrenheit(celcius):
    return (celcius * 9/5) + 32

def to_inches(mm):
    return mm / 25.4
