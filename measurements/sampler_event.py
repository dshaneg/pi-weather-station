class SamplerEvent(object):
    def __init__(self, timestamp, temperature, humidity, pressure):
        self.timestamp = timestamp
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
