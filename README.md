# Pi Weather Station

Polls the sensors on the Sense Hat, then updates the LED grid with the temperature and publishes the data on a regular interval over UDP.

## Design

```mermaid
graph TD;
    main -->|start| Sampler
    Sampler -->|weather event| pubsub([weather topic]);
    Sampler -->|reads sensors| SenseHat;
    pubsub -->|weather event| Broadcaster;
    pubsub -->|weather event| HatUpdater;
    Broadcaster -->|publishes event| udp([UDP]);
    HatUpdater --> HatController;
    HatController -->|reads stick & updates leds| SenseHat;
    HatController-->|prev/next/paint|PageService;
    PageService --> DigitsPage;
    PageService --> FacesPage;
    DigitsPage --> DigitsThemes;
    DigitsPage -->|updates leds| SenseHat;
    FacesPage -->|updates leds| SenseHat;
```

## Setup

If you haven't already, you need to make sure the `sense-hat` libraries are installed. (see [sense-hat](https://pythonhosted.org/sense-hat/))

```sh
sudo apt-get udpate
sudo apt-get install sense-hat
sudo reboot
```

To get this working on Raspbian (see [NumPy troubleshooting page](https://numpy.org/devdocs/user/troubleshooting-importerror.html))

```sh
sudo apt-get install libatlas-base-dev
```

Now install your other dependencies. You should only need to do this once. These are global installs because I couldn't make sense-hat cooperate with pipenv.

```sh
pipenv install pypubsub
```

Enter the virtual environment. Do this for every session.

```sh
pipenv shell
```

### Usage

To kick it off:

```sh
make start
```

In addition to updating the display on the hat, the application publishes messages over UDP. To listen to those messages, in another shell, or on another computer in the local network:

```sh
make listen
```

### systemd

I installed the application so that it runs at startup as a systemd service. To replicate, do the following:

Create a service file definition in `/etc/systemd/system`. I named mine `pi-weather.service`. The `.service` suffix is required. I set the username to my own username so that the python libraries installed for me globally could be read. Otherwise it runs as `root`.

```ini
[Unit]
Description=Pi Weather Station
After=multi-user.target

[Service]
Type=idle
ExecStart=/usr/bin/python3 main.py
WorkingDirectory=/home/shane/src/pi-weather-station
User=shane

[Install]
WantedBy=multi-user.target
```

Once you've created the file, run the following to register the service and start it.

```bash
sudo systemctl daemon-reload # registers the service file (also do this if you make changes to the service file)

sudo systemctl enable pi-weather.service
sudo systemctl start pi-weather.service
```

The logger's level is set to WARNING, so not much will show in the logs by default, but to see the logs of the service when started as a systemd service, run the following.

```bash
journalctl -eu pi-weather
```