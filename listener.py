import json
import socket
from listener_reporter import ListenerReporter


def init_socket():
    client = socket.socket(
        socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    # enable port reuse so we can run multiple clients and servers on a single (host, port)
    client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    # enable udp broadcast on the socket
    client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    client.bind(("", 37020))

    return client


def get_next_message(client):
    data, addr = client.recvfrom(1024)
    data_string = data.decode()
    message = json.loads(data_string)
    return message


def main():
    reporter = ListenerReporter()
    client = init_socket()

    while True:
        message = get_next_message(client)
        reporter.print_temp(message)


if __name__ == "__main__":
    main()
