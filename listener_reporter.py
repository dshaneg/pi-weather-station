from datetime import datetime, timezone


class ListenerReporter(object):
    def __init__(self):
        self.__last_temp = None

    @staticmethod
    def __utc_to_local(utc_date):
        return utc_date.replace(tzinfo=timezone.utc).astimezone(tz=None)

    def print_temp(self, message):
        stamp = ListenerReporter.__utc_to_local(
            datetime.fromisoformat(message['timestamp']))
        current_temp = message['temperature']
        if self.__last_temp is not None:
            diff = current_temp - self.__last_temp
            print(f'{stamp:%I:%M:%S %p}: {current_temp}°F ({diff:+.1f})')
        else:
            print(f'{stamp:%I:%M:%S %p}: {current_temp}°F')

        self.__last_temp = current_temp
