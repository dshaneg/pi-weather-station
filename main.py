from sense_hat import SenseHat
import signal
import logging
from pubsub import pub

from hat_ui.pages.digits.digits_theme import themes as digits_themes
from hat_ui.pages.digits.digits_page import DigitsPage
from hat_ui.pages.faces.faces_page import FacesPage
from hat_ui.page_service import PageService
from hat_ui.hat_controller import HatController
from subscribers.broadcaster import Broadcaster
from subscribers.hat_updater import HatUpdater
from measurements.sampler import Sampler


def initialize_hat(sense_hat):
    sense_hat.set_rotation(0)
    sense_hat.clear()


def hook_signals(sense_hat):
    def sig_handler(signum, frame):
        initialize_hat(sense_hat)
        exit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGQUIT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGHUP, sig_handler)


def main():
    logging.basicConfig(level=logging.WARNING)
    logging.info('Starting pi-weather-station')

    sense_hat = SenseHat()
    hook_signals(sense_hat)
    initialize_hat(sense_hat)

    # set up the user interface
    pages = [DigitsPage(sense_hat, digits_themes), FacesPage(sense_hat)]
    page_service = PageService(pages)
    hat_controller = HatController(sense_hat, page_service)

    # set up the weather sampler subscribers and publisher
    topic = 'weather'

    # start up the local event handlers
    udp_broadcaster = Broadcaster(pub, topic)
    udp_broadcaster.start()

    hat_updater = HatUpdater(hat_controller, pub, topic)
    hat_updater.start()

    # start the main (local) event publisher
    poll_inverval_secs = 5
    sampler = Sampler(sense_hat, pub, topic)
    sampler.start(poll_inverval_secs)


if __name__ == "__main__":
    main()
